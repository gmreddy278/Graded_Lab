package lab.graded.capgemini.performance_software_api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GeneralPerformerTest {
	
	private Performer performer;
	
	@Before
	public void setUp() {
		performer  = new GeneralPerformer(11);
	}
	
	@Test
	public void testPerformerId() {
		assertEquals(11, performer.getUniqueId());
	}
	@Test
	public void testPerformamceType() {
		assertEquals("performer", performer.getPerformanceType());
	}

}
