package lab.graded.capgemini.performance_software_api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DancePerformerTest {

	private Performer performer;

	@Before
	public void setUp() {
		performer = new DancePerformer(21,"tap");
	}

	@Test
	public void testPerformerId() {
		assertEquals(21, performer.getUniqueId());
	}

	@Test
	public void testPerformamceType() {
		assertEquals("Dancer", performer.getPerformanceType());
	}
	
	@Test
	public void testStyleType() {
		assertEquals("tap", ((DancePerformer) performer).getStyle());
	}

}
