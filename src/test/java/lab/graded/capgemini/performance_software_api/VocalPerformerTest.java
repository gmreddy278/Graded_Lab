package lab.graded.capgemini.performance_software_api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VocalPerformerTest {

	private Performer performer;
	private Performer performerVolume;
	private Performer performerIllegalVolume;
	
	@Before
	public void setUp() {
		performer = new VocalPerformer(31, "G");
		performerVolume = new VocalPerformer(41, "E-Minor", 9);
		
	}

	@Test
	public void testPerformerId() {
		assertEquals(31, performer.getUniqueId());
	}

	@Test
	public void testKeyType() {
		assertEquals("G", ((VocalPerformer) performer).getKey());
	}
	
	@Test
	public void testVolume() {
		assertEquals(9, ((VocalPerformer) performerVolume).getVolume());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalVolume() {
		
		//the reason why i instantiated this object here is:
		// because if i initialize this one in before class, I am getting IllegalArgumentException,
		// since I am not handling that exception anywhere.
		// so it is better to initialize here.
		performerIllegalVolume = new VocalPerformer(41, "E-Minor", 11);
		((VocalPerformer) performerIllegalVolume).getVolume();
	}

}
