package lab.graded.capgemini.performance_software_api;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class PerformanceClient {

	private static Map<Integer, String> performers = new HashMap<Integer, String>();

	final static Logger logger = Logger.getLogger(PerformanceClient.class);

	public static void main(String[] args) {
		

		Performer general = new GeneralPerformer(11);
		general.perform();

		Performer general1 = new GeneralPerformer(12);
		general1.perform();

		Performer general2 = new GeneralPerformer(13);
		general2.perform();

		Performer general3 = new GeneralPerformer(14);
		general3.perform();

		Performer dancer = new DancePerformer(21, "tap");
		dancer.perform();

		Performer dancer1 = new DancePerformer(22, "ballet");
		dancer1.perform();

		Performer vocal = new VocalPerformer(31, "G");
		vocal.perform();

		Performer vocalVolume = new VocalPerformer(41, "G", 8);
		vocalVolume.perform();

		// 4 performers, 2 dancers and 1 vocalist
		logger.info(" ");
		logger.info(" ");
		logger.info(" ");
		logger.info(" ");
		logger.info(" ---------------------------------------------------------- -");
		logger.info("Please find below the list of performers:");
		logger.info(" ---------------------------------------------------------- -");
		addToList(general);
		addToList(general1);
		addToList(general2);
		addToList(general3);
		addToList(dancer);
		addToList(dancer1);
		addToList(vocal);
		addToList(vocalVolume);
		logger.info(" ---------------------------------------------------------- -");

		System.out.println("Please find below the list of performers:");
		System.out.println("----------------------");

		for (Integer key : performers.keySet()) {
			// iterate over key
			// to display with key
			// System.out.println(key+" "+performers.get(key).toString());
			// to display without key
			System.out.println(performers.get(key).toString());
		}

		System.out.println("----------------------");
		
		

	}

	static String addToList(Performer p) {
		if (performers.containsKey(p.getUniqueId())) {
			System.out.println();

			logger.info(" *********************************************************");
			logger.error(
					"Please give another unique Id as id  --- " + p.getUniqueId() + " --- is already taken by others.");
			logger.info(" *********************************************************");
			return "Please give another unique Id as id  --- " + p.getUniqueId() + " --- is already taken by others.";
		} else {
			performers.put(p.getUniqueId(), p.perform());

			logger.info(p.perform() + " added to performers list");

			return "added to performers list";
		}
	}
}
