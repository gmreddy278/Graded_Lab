package lab.graded.capgemini.performance_software_api;

public class DancePerformer extends Performer {

	private String Style;

	public DancePerformer(int uniqueId, String style) {
		super(uniqueId);
		this.setStyle(style);
		this.setPerformanceType("Dancer");
	}

	

	public String getStyle() {
		return Style;
	}

	public void setStyle(String style) {
		Style = style;
	}

	//“tap - 772  - dancer”
	
	@Override
	String perform() {
		//System.out.println("\""+ getStyle() + " - " + getUniqueId() + " - "+ getPerformanceType() + "\"");
		return ("\""+ getStyle() + " - " + getUniqueId() + " - "+ getPerformanceType() + "\"");
	}



	@Override
	public String toString() {
		return "DancePerformer [Style=" + Style + ", UniqueId=" + getUniqueId() + ", PerformanceType="
				+ getPerformanceType() + "]";
	}
	
	

}
