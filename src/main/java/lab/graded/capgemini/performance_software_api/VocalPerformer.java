package lab.graded.capgemini.performance_software_api;

public class VocalPerformer extends Performer {

	private String key;
	private int volume;

	public VocalPerformer() {
		super();
		
	}

	public VocalPerformer(int uniqueId, String key) {
		this.setUniqueId(uniqueId);
		setKey(key);
	}

	public VocalPerformer(int uniqueId, String key, int volume) {
		this.setUniqueId(uniqueId);
		setKey(key);
		setVolume(volume);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {

		if (volume < 1 || volume > 10) {

			throw new IllegalArgumentException("Volume should be in the range of [1,10]");
		}
		this.volume = volume;

	}

	// “I sing in the key of – G – 1191”

	// I sing in the key of – G – at the volume 5 - 1245 ”
	@Override
	String perform() {
		if (getVolume() == 0) {
			
			//System.out.println("I sing in the key of – " + getKey() + "  - " + getUniqueId());
			return ("\""+ "I sing in the key of – " + getKey() + "  - " + getUniqueId()+ "\"");
		}
		else
			//System.out.println("I sing in the key of – " + getKey() + "  -  at the volume " + getVolume()+ "  -  "+getUniqueId());
			return ("\"" + "I sing in the key of – " + getKey() + "  -  at the volume " + getVolume()+ "  -  "+getUniqueId() + "\"");
	}

}
