package lab.graded.capgemini.performance_software_api;

public class GeneralPerformer extends Performer{
	
	
	
	/*public GeneralPerformer() {
		
		super.setPerformanceType("performer");
	}*/
	
	public GeneralPerformer(int uniqueId) {
		super(uniqueId);
		super.setPerformanceType("performer");
	}

	@Override
	String perform() {
		//System.out.println("\""+ getUniqueId() +"-" + getPerformanceType()+"\"");
		
		return ("\""+ getUniqueId() +"-" + getPerformanceType()+"\"");
		
	}

	@Override
	public String toString() {
		return "GeneralPerformer [UniqueId=" + getUniqueId() + ", PerformanceType=" + getPerformanceType()
				+ "]";
	}
	
	

}
