package lab.graded.capgemini.performance_software_api;

public abstract class Performer {

	private int uniqueId;
	private String performanceType;
	
	

	public Performer() {
		super();
	}

	public Performer(int uniqueId) {
		super();
		this.uniqueId = uniqueId;
	}

	
	
	public Performer(int uniqueId, String performanceType) {
		super();
		this.uniqueId = uniqueId;
		this.performanceType = performanceType;
	}

	public int getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getPerformanceType() {
		return performanceType;
	}

	public void setPerformanceType(String performanceType) {
		this.performanceType = performanceType;
	}

	abstract String perform();

	@Override
	public String toString() {
		return "Performer [uniqueId=" + uniqueId + ", performanceType=" + performanceType + "]";
	}

	
	
}
